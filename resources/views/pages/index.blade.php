@extends('layouts.app')

@section('content')    
    <div class="jumotron text-center">
        <h1>{{$title}}</h1>
        <p>This is PJAPP 'Main' Webpage application from the "Laravel Tutorial" Youtube series</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a>
            <a class="btn btn-success btn-lg" href="/register" role="button">Register</a></p>
    </div>
@endsection
